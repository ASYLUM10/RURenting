﻿var isLoggedIn;
var isAdmin;
var isAgent;
var isStudent;
var username;

function GlobalOnLoad() {
    isLoggedIn = ((document.getElementById('LoggedIn') != null ) ? true : false );
    var display = document.getElementById('LoggedInAction');
    if (isLoggedIn) {
        //user is logged in
        username = document.getElementById('LoggedInUsername').textContent;
        isAdmin = ((document.getElementById('LoggedInAdmin').textContent == 'Yes') ? true : false);
        isAgent = ((document.getElementById('LoggedInAgent').textContent == 'Yes') ? true : false);
        isStudent = ((document.getElementById('LoggedInStudent').textContent == 'Yes') ? true : false);
        display.innerHTML = '<p>Logged in as: ' + username + '<br/><a href="https://c3dzk285.caspio.com/folderlogout" id="Logout">Log Out</a></p>';
        if (!isAdmin)
            document.getElementById('btnAdmin').style.display = "none";
    }
    else {
        //user is not logged in
        display.innerHTML = '<a href="Register.html" id="Register">Register</a>&nbsp<a href="Login.html" id="Login">Login</a>';
        document.getElementById('btnBasicDetails').style.display = "none";
        document.getElementById('btnAdmin').style.display = "none";
        document.getElementById('btnMessages').style.display = "none";
    }

    window.scrollTo(0, 0);
}