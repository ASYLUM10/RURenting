﻿function PopulatePage() {
    var time = performance.now();
    var nodes = document.getElementById('cDigsDetails');
    if (nodes == null) {
        document.getElementById('Loading').style.display = 'none';
        document.getElementById('Error').style.display = 'flex';
        return;
    }
    PopulateDetails();
    ViewReviews();
    NewReview();
    document.getElementById('NewReview').getElementsByTagName('form')[0].onsubmit = StoreIndividualScores;
    console.log(performance.now() - time + ' ms'); //Proof that Caspio is slow, not my code :D
    document.getElementById('Loading').style.display = 'none';
    document.getElementById('MainContainer').style.display = 'Inline';
}

var digsID, owner;
function PopulateDetails() {
    var name, unit;
    var nodes = document.getElementById('cDigsDetails').children;
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].id == 'cDigsID')
            digsID = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cOwner')
            owner = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cDigsName')
            name = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cUnitNo')
            unit = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cDigsAddress')
            document.getElementById('Address').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cPrice')
            document.getElementById('Price').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cNoBedrooms')
            document.getElementById('Bedrooms').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cNoBathrooms')
            document.getElementById('Bathrooms').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cNoGarages')
            document.getElementById('Garages').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cSwimmingPool')
            document.getElementById('Pool').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cPetFriendly')
            document.getElementById('Pets').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cMeterSquared')
            if (nodes[i].innerText.trim() == '') document.getElementById('Size').parentElement.style.display = 'none';
            else document.getElementById('Size').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cDetails')
            document.getElementById('Details').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cOtherFacilities')
            document.getElementById('Facilities').innerHTML = (nodes[i].innerText.trim().length > 0) ? nodes[i].innerText.trim() : 'N/A';
        else if (nodes[i].id == 'cIsComplex')
            document.getElementById('Complex').innerHTML = nodes[i].innerText.trim();
        else if (nodes[i].id == 'cImage')
            document.getElementById('Image').src = nodes[i].innerText.trim();
    }
    document.getElementById('DigsName').innerHTML = unit + ((unit.length > 0) ? ' ' : '') + name;
}

function ViewReviews() {
    var score, stars;
    var reviews = document.getElementsByClassName('ViewReview');
    for (var a = 0; a < reviews.length; a++) {
        var nodes = reviews[a].children;
        for (var b = 0; b < nodes.length; b++) {
            if (nodes[b].className == 'ViewScore')
                score = nodes[b].innerText.trim();
            else if (nodes[b].className == 'ViewStars')
                stars = nodes[b];
        }
        var dummyScore = score;
        score = (Math.round(score * 2) / 2).toFixed(1);
        var p = document.createElement('span');
        p.className = 'OverallLabel';
        var b = document.createElement('b');
        b.innerHTML = 'Overall Rating:';
        p.appendChild(b);
        stars.appendChild(p);
        var row = document.createElement('div');
        for (var i = 0; i < 5; i++) {
            var star = document.createElement('img');
            star.className = 'Star';
            if (score >= 1.0) {
                star.src = '../Images/FullStar.png';
                star.alt = 'Full Star';
                score -= 1;
            }
            else if (score >= 0.5) {
                star.src = '../Images/HalfStar.png';
                star.alt = 'Half Star';
                score -= 0.5;
            }
            else {
                star.src = '../Images/EmptyStar.png';
                star.alt = 'Empty Star';
            }
            row.appendChild(star);
        }
        stars.appendChild(row);
        var p = document.createElement('p');
        p.innerHTML = Math.round(dummyScore * 100) / 100;
        stars.appendChild(p);
    }
    ViewReviewCriteria();
}

function ViewReviewCriteria() {
    var digsIDs = []; var digsRatings = []; var digsName = [];
    var id = ''; var totalScore;
    var groups = [].slice.call(document.querySelectorAll('#CriteriaRatings form tr'));
    for (var i = 0; i < groups.length; i++) {
        var children = [].slice.call(groups[i].children);
        for (var a = 0; a < children.length; a++) {
            if (children[a].className.includes('cbResultSetGroup1LabelCell') && !children[a].className.includes('NumberDate') && children[a].innerText.trim() != '')
                id = children[a].innerText.trim();
            else if (children[a].className.includes('cbResultSetTableCellNumberDate')) {
                digsRatings.push(children[a].innerText.trim());
                digsIDs.push(id);
            }
            else if (children[a].className.includes('cbResultSetTableCell'))
                digsName.push(children[a].innerText.trim());
            else if (children[a].className.includes('cbResultSetTotalsDataCellNumberDate'))
                totalScore = children[a].innerText.trim();
        }
    }
    var list = document.getElementsByClassName('Criteria');
    for (var i = 0; i < list.length; i++) {
        var found = false;
        var index = -1;
        var id = list[i].children[0].innerText.trim();
        index = digsIDs.indexOf(id);
        var labels = document.createElement('div');
        var stars = document.createElement('div');
        while (index != -1) {
            var p = document.createElement('p');
            p.className = 'CriteriaLabel';
            p.innerHTML = digsName[index];
            labels.appendChild(p);
            found = true;
            var row = document.createElement('div');
            var score = (Math.round(digsRatings[index] * 2) / 2).toFixed(1);
            for (var j = 0; j < 5; j++) {
                var star = document.createElement('img');
                star.className = 'SmallStar';
                if (score >= 1.0) {
                    star.src = '../Images/FullStar.png';
                    star.alt = 'Full Star';
                    score -= 1;
                }
                else if (score >= 0.5) {
                    star.src = '../Images/HalfStar.png';
                    star.alt = 'Half Star';
                    score -= 0.5;
                }
                else {
                    star.src = '../Images/EmptyStar.png';
                    star.alt = 'Empty Star';
                }
                row.appendChild(star);
            }
            stars.appendChild(row);
            list[i].appendChild(labels);
            list[i].appendChild(stars);
            index = ((index == -1) ? digsIDs.indexOf(id) : digsIDs.indexOf(id, index + 1));
        }
        if (!found) {
            var p = document.createElement('p');
            p.innerHTML = 'Error :(';
            list[i].appendChild(p);
        }
    }
    OverallScore(totalScore);
}

function OverallScore(digsRating) {
    var display = document.getElementById('OverallRating');
    if (digsRating != '') {
        var score = (Math.round(digsRating * 2) / 2).toFixed(1)
        var stars = document.createElement('span');
        for (var j = 0; j < 5; j++) {
            var star = document.createElement('img');
            star.className = 'Star';
            if (score >= 1.0) {
                star.src = '../Images/FullStar.png';
                star.alt = 'Full Star';
                score -= 1;
            }
            else if (score >= 0.5) {
                star.src = '../Images/HalfStar.png';
                star.alt = 'Half Star';
                score -= 0.5;
            }
            else {
                star.src = '../Images/EmptyStar.png';
                star.alt = 'Empty Star';
            }
            stars.appendChild(star);
        }
        display.appendChild(stars);
        p = document.createElement('p');
        p.innerHTML = ' ' + Math.round(digsRating * 100) / 100;
        if (isNaN(digsRating)) {
            p = document.createElement('p');
            p.innerHTML = 'No Reviews';
        }
    }
    else {
        p = document.createElement('p');
        p.innerHTML = 'No Reviews';
    }
    display.appendChild(p);
}

function NewOverallStars(node) {
  var row = document.createElement('div');
  row.className = 'NewOverallStars';
  var span = document.createElement('span');
  var b = document.createElement('b');
  b.className = 'OverallLabel';
  b.innerHTML = 'Overall Score:&nbsp;&nbsp;';
  row.appendChild(b);
  for (var i = 0; i < 5; i++) {
      var star = document.createElement('img');
      star.src = (i == 0) ? '../Images/FullStar.png' : '../Images/EmptyStar.png';
      star.className = 'Star';
      star.id = 'Star_Overall_' + i;
      row.appendChild(star);
  }
  var s = document.createElement('span');
  s.className = 'OverallScore';
  s.id = 'Score_Overall';
  s.innerHTML = '1';
  s.style.marginLeft = '5px';
  row.appendChild(s);
  node.appendChild(row);
}

function NewCriteriaStars (node) {
  var div = document.createElement('div');
  div.className = 'NewCriteriaStars';
  var labels = document.createElement('div');
  var values = document.createElement('div');
  var criteria = [].slice.call(document.querySelectorAll('[data-cb-name=data]'));
  criteria = criteria.filter(function (element) { return element.children.length == 1; }).map(function (element) { return element.innerText.trim() });
  for (var i = 0; i < criteria.length; i++) {
      var p = document.createElement('p');
      p.className = 'CriteriaLabelNew';
      p.innerHTML = criteria[i] + ':&nbsp;&nbsp;';
      p.id = 'Label_' + i;
      labels.appendChild(p);
      var stars = document.createElement('div');
      for (var a = 0; a < 5 ; a++) {
          var star = document.createElement('img');
          star.src = (a == 0) ? '../Images/FullStar.png' : '../Images/EmptyStar.png';
          star.className = 'SmallStar';
          star.id = 'Star_' + i + '_' + a;
          star.onmouseenter = function () { SelectStar(this, criteria.length, false); }
          star.onclick = function () { DelayStar(this, criteria.length); }
          stars.appendChild(star);
      }
      var s = document.createElement('span');
      s.className = 'CriteriaScore';
      s.id = 'Score_' + i;
      s.innerHTML = '1';
      s.style.marginLeft = '5px';
      stars.appendChild(s);
      values.appendChild(stars);
  }
  div.appendChild(labels);
  div.appendChild(values);
  node.appendChild(div);
  SetUpDelays(criteria.length);
}

function NewReview() {
    if (!IsValidUser()) return;
    document.getElementById('IncorrectUserMsg').style.display = 'none';
    var node = document.getElementById('NewStars');
    NewOverallStars(node);
    NewCriteriaStars(node);
}

function DisplayError(message) { document.getElementById('IncorrectUserMsg').innerHTML = 'Error: ' + message; }
function ShowAddReview() { document.getElementById('AddReview').style.display = 'inline'; }
function HasReviewedBefore() { return document.getElementById('PastSubmissions').getElementsByTagName('form')[0].getElementsByTagName('table').length > 0; }
function HasRentedDigs() { return document.getElementById('DigsRented').getElementsByTagName('form')[0].getElementsByTagName('table').length > 0; }

function IsValidUser() {
    var valid = false;
    if (!isLoggedIn)
        DisplayError('You must be logged in to leave a review');
    else if (!isStudent)
        DisplayError('You must be a student in order to leave a review');
    else if (HasReviewedBefore())
        DisplayError('You have already made a review for this digs');
    else if (!HasRentedDigs())
        DisplayError('You must have rented the digs before in order to rate it');
    else {
        ShowAddReview();
        valid = true;
    }
    return valid;
}

var DELCONST = 500;
var delay = [];
function SetUpDelays(n) {
    for (var i = 0; i < n; i++)
        delay.push(new Date(new Date() - DELCONST));
}

function DelayStar(star, n) {
    var row = star.id.substring(5, 6);
    SelectStar(star, n, true);
    delay[row] = new Date();
}

function SelectStar(star, n, ignoreDelay) {
    var row = star.id.substring(5, 6);
    var col = star.id.substring(7, 8);
    if (!ignoreDelay) {
        if (new Date() - delay[row] < DELCONST) return;
    }
    for (var i = 0; i < 5; i++)
        if (i <= col)
            document.getElementById('Star_' + row + '_' + i).src = '../Images/FullStar.png';
        else
            document.getElementById('Star_' + row + '_' + i).src = '../Images/EmptyStar.png';
    document.getElementById('Score_' + row).innerHTML = Number(col) + 1;
    var score = 0.00;
    for (var i = 0; i < n; i++)
        score += Number(document.getElementById('Score_' + i).innerHTML);
    score = Math.round(score / n * 100) / 100;
    document.getElementById('Score_Overall').innerHTML = score;
    document.getElementById('InsertRecordmaxScore').value = score;
    score = (Math.round(score * 2) / 2).toFixed(1);
    for (var i = 0; i < 5; i++) {
        if (score >= 1.0) {
            document.getElementById('Star_Overall_' + i).src = '../Images/FullStar.png';
            score -= 1;
        }
        else if (score >= 0.5) {
            document.getElementById('Star_Overall_' + i).src = '../Images/HalfStar.png';
            score -= 0.5;
        }
        else document.getElementById('Star_Overall_' + i).src = '../Images/EmptyStar.png';
    }
}

function StoreIndividualScores() {
    var labels = document.getElementsByClassName('CriteriaLabelNew');
    var numStoredScores = labels.length;
    for (var i = 0; i < numStoredScores; i++) {
        document.getElementById('cbParamVirtual1').value += labels[i].innerText.trim().substr(0, labels[i].innerText.trim().length - 1) + (i == numStoredScores - 1 ? '' : ',');
        document.getElementById('cbParamVirtual2').value += document.getElementById('Score_' + labels[i].id.substring(6, 7)).innerText.trim() + (i == numStoredScores - 1 ? '' : ',');
    }
}

function MessageButton() { location.href = 'NewMessage.html?reciever=' + owner + '&responseTo=&cbResetParam=1'; }
function DigsReportButton() { location.href = 'ReportDigs.html?digsID=' + digsID; }
function DigsRentedButton() { location.href = 'AddDigsRented.html?digsID=' + digsID + '&cbResetParam=1'; }
function ReportButton() { location.href = 'ReportDigs.html?digsID=' + digsID + '&cbResetParam=1'; }

function DisableTools() {
    function DisableButton(button, message) {
        var btn = document.getElementById('btn' + button);
        btn.disabled = true;
        btn.title = message;
        btn.className = 'ToolBtn DisabledToolBtn';
    }
    if (!isLoggedIn) {
        DisableButton('Message', 'You must be logged in to send messages.');
        DisableButton('Report', 'You must be logged in to report.');
        DisableButton('DigsRented', 'You must be logged in to add to Digs Rented.');
    }
    else if (!isStudent) {
        DisableButton('Message', 'You must be a student to send messages.');
        DisableButton('DigsRented', 'You must be a student to add to Digs Rented.');
    }
}

function DisplayEdit(id, currentlyRenting) { return; }

function DisplayModal() { document.getElementById('ImagesModal').style.display = 'block'; }

function CloseModal() { document.getElementById('ImagesModal').style.display = 'none'; }

window.onclick = function (event) {
    if (event.target == document.getElementById('ImagesModal'))
        document.getElementById('ImagesModal').style.display = 'none';
}

//function ViewReviewCriteria() {
//    var digsIDs = [];
//    var digsRatings = [];
//    var digsName = [];
//    var groups = [].slice.call(document.querySelectorAll('[data-cb-name=data]'));
//    groups = groups.filter(function (element) { return element.children.length != 1; });
//    for (var i = 0; i < groups.length; i++) {
//        var id = '';
//        var rate = '';
//        var name = '';
//        var nodes = groups[i].children;
//        for (var a = 0; a < nodes.length; a++) {
//            if (nodes[a].className.includes('cbResultSetTableCellNumberDate')) {
//                var text = nodes[a].innerText.trim();
//                if (text.includes('.'))
//                    rate = text;
//                else id = text;
//            }
//            else if (nodes[a].className.includes('cbResultSetTableCell'))
//                name = nodes[a].innerText.trim();
//        }
//        if (id != '') {
//            digsIDs.push(id);
//            digsRatings.push(rate);
//            digsName.push(name);
//        }
//    }
//    var list = document.getElementsByClassName('Criteria');
//    for (var i = 0; i < list.length; i++) {
//        var found = false;
//        var index = -1;
//        var id = list[i].children[0].innerText.trim();
//        index = digsIDs.indexOf(id);
//        var labels = document.createElement('div');
//        var stars = document.createElement('div');
//        while (index != -1) {
//            var p = document.createElement('p');
//            p.className = 'CriteriaLabel';
//            p.innerHTML = digsName[index];
//            labels.appendChild(p);
//            found = true;
//            var row = document.createElement('div');
//            var score = (Math.round(digsRatings[index] * 2) / 2).toFixed(1);
//            for (var j = 0; j < 5; j++) {
//                var star = document.createElement('img');
//                star.className = 'SmallStar';
//                if (score >= 1.0) {
//                    star.src = '../Images/FullStar.png';
//                    star.alt = 'Full Star';
//                    score -= 1;
//                }
//                else if (score >= 0.5) {
//                    star.src = '../Images/HalfStar.png';
//                    star.alt = 'Half Star';
//                    score -= 0.5;
//                }
//                else {
//                    star.src = '../Images/EmptyStar.png';
//                    star.alt = 'Empty Star';
//                }
//                row.appendChild(star);
//            }
//            stars.appendChild(row);
//            list[i].appendChild(labels);
//            list[i].appendChild(stars);
//            index = ((index == -1) ? digsIDs.indexOf(id) : digsIDs.indexOf(id, index + 1));
//        }
//        if (!found) {
//            var p = document.createElement('p');
//            p.innerHTML = 'Error :(';
//            list[i].appendChild(p);
//        }
//    }
//}
