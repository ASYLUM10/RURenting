﻿var map;
var marker;
var SelectedPin;

function initializeMap() {
    SelectedPin = {
        url: "http://darqdev.co.za/Images/Unselected.png",
        size: new google.maps.Size(32, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 32)
    };

    var myLatlng = new google.maps.LatLng(-33.315133, 26.5207898);
    var myOptions = {
        zoom: 15,
        center: myLatlng,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);

    google.maps.event.addListener(map, 'click', function (event) {
        if (marker != null) marker.setMap(null);;
        var newLatLng = event.latLng
        placeMarker(newLatLng, false, SelectedPin);
        document.getElementById("InsertRecordlatitude").value = parseFloat(newLatLng.lat().toFixed(11));
        document.getElementById("InsertRecordlongitude").value = parseFloat(newLatLng.lng().toFixed(11));
    });

    document.getElementById('InsertRecordlatitude').addEventListener("focusout", function () {
        var lat = document.getElementById('InsertRecordlatitude').value;
        var lng = document.getElementById('InsertRecordlongitude').value;
        if (!isNaN(parseFloat(lat)) && !isNaN(parseFloat(lng))) {
            var newLatLng = new google.maps.LatLng(lat, lng);
            if (marker != null) marker.setMap(null);
            placeMarker(newLatLng, true, SelectedPin);
        }
    });

    document.getElementById('InsertRecordlongitude').addEventListener("focusout", function () {
        var lat = document.getElementById('InsertRecordlatitude').value;
        var lng = document.getElementById('InsertRecordlongitude').value;
        if (!isNaN(parseFloat(lat)) && !isNaN(parseFloat(lng))) {
            var newLatLng = new google.maps.LatLng(lat, lng);
            if (marker != null) marker.setMap(null);
            placeMarker(newLatLng, true, SelectedPin);
        }
    });

    google.maps.event.addDomListener(window, 'resize', function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(center);
    });
}

function placeMarker(location, center, pinImage) {
    marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: pinImage
    });
    if (center)
        map.setCenter(location);
}