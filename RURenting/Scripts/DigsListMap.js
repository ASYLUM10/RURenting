var map;
var prevDigs;
var markers = [];
var UnselectedPin;
var SelectedPin;

function initializeMap() {
    UnselectedPin = {
        url: "http://darqdev.co.za/Images/Unselected.png",
        size: new google.maps.Size(32, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 32)
    };
    SelectedPin = {
        url: "http://darqdev.co.za/Images/Selected.png",
        size: new google.maps.Size(32, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 32)
    };

    var myLatlng = new google.maps.LatLng(-33.315133, 26.5207898);
    var myOptions = {
        zoom: 15,
        center: myLatlng,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);
    addMarkers();
}

function OnEnter(digsID) {
    if (prevDigs != null) {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i].digsID == prevDigs)
                markers[i].marker.setIcon(UnselectedPin);
        }
    }
    prevDigs = digsID;
    for (var i = 0; i < markers.length; i++) {
        if (markers[i].digsID == digsID)
            markers[i].marker.setIcon(SelectedPin);
    }
}

function addMarkers() {
    for (var i = markers.length - 1; i >= 0; i--) {
        var marker = markers[i].marker.setMap(null);
        markers.pop();
    }
    var digs = document.getElementsByClassName("Location");

    for (var i = 0; i < digs.length; i++) {
        var nodes = digs[i].children;
        var digsID = "", lat = 0.0, lng = 0.0;
        for (var j = 0; j < nodes.length; j++) {
            if (nodes[j].id == "DigsID") {

                digsID = nodes[j].innerText.trim();
            }
            else if (nodes[j].id == "Latitude") {
                lat = nodes[j].innerText.trim();
            }
            else if (nodes[j].id == "Longitude") {
                lng = nodes[j].innerText.trim();
            }
        }
        placeMarker(new google.maps.LatLng(lat, lng), digsID, UnselectedPin);
    }
}

function placeMarker(location, _digsID, _pinImage) {
    _marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: _pinImage,
        url: "DigsDetails.html?digsID=" + _digsID
    });
    var markerStruct = {
        digsID: _digsID,
        marker: _marker
    }
    markers.push(markerStruct);

    (function (marker) {
        google.maps.event.addListener(marker, "click", function (e) {
            window.location.href = marker.url;
        });
    })(_marker);
}

