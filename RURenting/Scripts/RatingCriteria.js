﻿function ActivateButtons() {
    var Activate = document.querySelectorAll('.cActivate');
    var Deactivate = document.querySelectorAll('.cDeactivate');
    var IsActive = document.querySelectorAll('.cIsActive');
    var Criteria = document.querySelectorAll('.cCriteria');
    // Set their ids
    for (var i = 0; i < Activate.length; i++) {
        Activate[i].id = 'cActivate' + i;
        Deactivate[i].id = 'cDeactivate' + i;
        IsActive[i].id = 'cIsActive' + i;
        Criteria[i].id = 'cCriteriaName' + i;
    }
    //Activate or Deactivate
    for (var i = 0; i < Activate.length; i++) {
        var dummy = document.getElementById('cIsActive' + i).innerHTML;
        if (dummy == "Yes") {
            document.getElementById('cDeactivate' + i).style.display = "block";
        }
        else {
            document.getElementById('cActivate' + i).style.display = "block";
        }
    }
}
document.onload = ActivateButtons();

function checkActivate(id) {
    var IsActive = document.querySelectorAll('.cIsActive');
    var count = 0;
    var num = id.substr(id.length - 1);
    for (var i = 0; i < IsActive.length; i++) {
        var dummy = document.getElementById('cIsActive' + i).innerHTML;
        if (dummy == "Yes")
            count++
    }
    if (count >= 4) {
        window.alert("There can be no more than 4 active criteria at any given time.");
        return false;
    }
    var url = "EditRatingCriteria.html?lastModifiedBy=" + username + "&setActive=Yes&criteriaName=" + document.getElementById('cCriteriaName' + num).innerHTML;
    window.location.href = url;
}

function checkDeactivate(id) {
    var IsActive = document.querySelectorAll('.cIsActive');
    var count = 0;
    var num = id.substr(id.length - 1);
    for (var i = 0; i < IsActive.length; i++) {
        var dummy = document.getElementById('cIsActive' + i).innerHTML;
        if (dummy == "Yes")
            count++
    }
    if (count <= 2) {
        window.alert("There needs to be at least 2 active criteria at any given time.");
        return false;
    }
    var url = "EditRatingCriteria.html?lastModifiedBy=" + username + "&setActive=No&criteriaName=" + document.getElementById('cCriteriaName' + num).innerHTML;
    window.location.href = url;
}

function checkNewRecord() {
    var name = document.getElementsByName("InlineAddcriteriaName");
    if (name.length > 15) {
        window.alert("Criteria Name has a maximum length of 15 characters.");
        return false;
    }
    var IsActive = document.querySelectorAll('.cIsActive');
    var count = 0;
    for (var i = 0; i < IsActive.length; i++) {
        var dummy = document.getElementById('cIsActive' + i).innerHTML;
        if (dummy == "Yes")
            count++
    }
    var checked = document.getElementById('InlineAddisActive').checked;
    if (checked) {
        if (count >= 4) {
            window.alert("There can be no more than 4 active criteria at any given time.");
            return false;
        }
    }
    return true;
}

document.getElementById('Mod0InlineAdd').onclick = function () { return checkNewRecord() };