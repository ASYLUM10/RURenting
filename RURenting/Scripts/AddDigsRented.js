﻿function DisplayEdit(a, b) {

}

function checkDate() {
    var currentDate = Date.now();
    var movedIn = Date.parse(document.getElementById('InsertRecorddateMovedIn').value);
    var movedOut = Date.parse(document.getElementById('InsertRecorddateMovedOut').value);
    if (movedIn >= currentDate) {
        window.alert("Date Moved In cannot be after the present date.");
        return false;
    }
    if (!isNaN(movedOut)) {
        var timeDiff = Math.abs(movedOut - movedIn);
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (movedOut > currentDate) {
            window.alert("Date Moved Out cannot be after the present date.");
            return false;
        }
        else if (movedOut <= movedIn) {
            window.alert("Date Moved In must be before the date Moved Out.");
            return false;
        }
        else if (diffDays < 7) {
            window.alert("The minimum time period for staying in a digs is 7 days.");
            return false;
        }
    }
    var digsRented = document.getElementsByClassName("RentedDigs");
    for (var i = 0; i < digsRented.length; i++) {
        var nodes = digsRented[i].children;
        var cRenting = "", cMoveIn = 0, cMoveOut = 0.0;
        for (var j = 0; j < nodes.length; j++) {
            if (nodes[j].id == "cCurrentlyRenting") {
                cRenting = nodes[j].innerText.trim();
            }
            else if (nodes[j].id == "cMovedIn") {
                cMoveIn = Date.parse(nodes[j].innerText);
            }
            else if (nodes[j].id == "cMovedOut") {
                cMoveOut = Date.parse(nodes[j].innerText);
            }
        }
        if (cRenting == "Yes" && isNaN(movedOut)) {
            window.alert("You are currently renting another digs.");
            return false;
        }
        else if (cRenting == "Yes" && !!(movedIn > cMoveIn || movedOut > cMoveIn)) {
            window.alert("You are currently renting another digs.");
            return false;
        }
        else if (isNaN(movedOut) && movedIn < cMoveOut) {
            window.alert("The move in date is after the move out date for another digs you have rented.");
            return false;
        }
        if (!isNaN(movedOut) && !isNaN(cMoveOut)) {
            if (movedIn >= cMoveIn && movedIn <= cMoveOut) {
                window.alert("Date Moved In is within a period in which you rented another digs.");
                return false;
            }
            else if (movedOut >= cMoveIn && movedOut <= cMoveOut) {
                window.alert("Date Moved Out is within a period in which you rented another digs.");
                return false;
            }
            else if (!(movedIn > cMoveOut) && !(movedOut < cMoveIn)) {
                window.alert("The dates you have set go over a time period within which you rented another digs");
                console.log(!(movedIn > cMoveOut));
                console.log(!(movedOut < cMoveIn));
                return false;
            }
        }
    }
    if (isNaN(movedOut))
        document.getElementById('InsertRecordcurrentlyRenting').checked = true;
    else
        document.getElementById('InsertRecordcurrentlyRenting').checked = false;
    return true;
}

document.getElementById('caspioform').onsubmit = function () { return (checkDate()) };
