﻿var TABS = {
    BASIC: 'BasicDetails',
    PASSWORD: 'ChangePassword',
    STUDENT: 'StudentDetails',
    AGENT: 'AgentDetails',
    MESSAGES: 'Messages'
};

function AccountOnLoad () {
    DisplayTab(TABS.BASIC);
}

function DisplayTab(tab) {
    for (t in TABS) {
        document.getElementById(TABS[t]).style.display = 'none';
        var button = document.getElementById("btn" + TABS[t]);
        button.style.backgroundColor = '#00599A';
        button.style.color = 'white';
        
        button.onmouseenter = function () {
            if (this.style.background != 'white')
                this.style.backgroundColor =  '#002A3A';
        }

        button.onmouseleave = function () {
            if (this.style.background != 'white')
                this.style.backgroundColor = '#00599A';
        }
    }
    document.getElementById(tab).style.display = 'block';
    var button = document.getElementById('btn' + tab);
    button.style.background = 'white';
    button.style.color = '#00599A';
}