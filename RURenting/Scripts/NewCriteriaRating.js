﻿// http://localhost:56638/pages/NewCriteriaRating.html?digsID=1&ratingID=40&names=Facilities%2cService%2cCondition%2cTest&scores=1%2c3%2c2%2c5&cbResetParam=1
// NewCriteriaRating.html?digsID=[@field:digsID]&ratingID=[@field:ratingID]&names=[@cbParamVirtual1]&scores=[@cbParamVirtual2]&cbResetParam=1
function PopulateCriteria() {
    var id, names = [], scores = [];
    var params = window.location.href.substring(window.location.href.indexOf('?') + 1);
    params = params.split('&');
    for (var i = 0; i < params.length; i++) {
        var att = params[i].substring(0, params[i].indexOf('='));
        var val = params[i].substring(params[i].indexOf('=') + 1);
        while (val.indexOf('+') != -1) val = val.replace('+', ' ');
        if (att == 'ratingID') id = val;
        else if (att == 'names') names = val.split('%2c');
        else if (att == 'scores') scores = val.split('%2c');
    }
    if (typeof (Storage) !== "undefined") {
        if (sessionStorage.getItem('ratingIDSubmitted') == '' + id) {
            history.go(-1);
            return;
        }
    } else {
        var cookies = document.cookie;
        cookies = cookies.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var keyValue = cookies[i].split('=');
            if (keyValue[0] == 'ratingIDSubmitted') {
                if (keyValue[1] == '' + id) {
                    history.go(-1);
                    return;
                }
                break;
            }
        }
    }
    for (var i = 0; i < names.length; i++)
        InsertRow(id, names[i], scores[i]);
    if (typeof (Storage) !== "undefined")
        sessionStorage.setItem('ratingIDSubmitted', '' + id);
    else document.cookie = 'ratingIDSubmitted=' + id;
    history.go(-1);
}

function InsertRow(id, name, score) {
    document.getElementsByName('InlineAddIndividualScore_ratingID')[0].value = id;
    document.getElementsByName('InlineAddIndividualScore_criteriaName')[0].value = name;
    document.getElementsByName('InlineAddIndividualScore_score')[0].value = score;
    document.getElementById('Mod0InlineAdd').click();
}