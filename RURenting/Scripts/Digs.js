﻿var advanced = false;
function ToggleAdvancedSearch() {
    advanced = !advanced;
    if (advanced) {
        document.getElementById('AdvancedSearch').style.display = 'block';
        document.getElementById('btnSearch').style.visibility = 'hidden';
        document.getElementById('btnClear').style.visibility = 'hidden';
        document.getElementById('btnAdvanced').value = '˄ Advanced Search';
    }
    else {
        document.getElementById('AdvancedSearch').style.display = 'none';
        document.getElementById('btnSearch').style.visibility = 'visible';
        document.getElementById('btnClear').style.visibility = 'visible';
        document.getElementById('btnAdvanced').value = '˅ Advanced Search';
        ClearAdvInputs();
    }
}

function Search() {
    if (document.getElementById('cbGarage').checked)
        document.getElementById('cbGarage').value = 1;
    else
        document.getElementById('cbGarage').value = '';
}

function ClearAdvInputs() {
    document.getElementById('txtAddress').value = '';
    document.getElementById('txtBeds').value = '';
    document.getElementById('txtBaths').value = '';
    document.getElementById('cbPool').checked = false;
    document.getElementById('cbPet').checked = false;
    document.getElementById('cbGarage').checked = false;
    document.getElementById('cbComplex').checked = false;
}

var possibleSearchCriteria = ['name', 'price', 'address', 'beds', 'baths', 'pool', 'pets', 'garage', 'complex'];
function GetCurrentSearch() {
    var criteria = document.getElementById('Criteria');
    var params = window.location.href.substring(window.location.href.indexOf('?') + 1);
    params = params.split('&');
    if (params.length < 2) return;
    for (var i = 0; i < params.length; i++) {
        if (params[i].indexOf('=') == params[i].length - 1) continue;
        var name = params[i].substring(0, params[i].indexOf('='));
        if (possibleSearchCriteria.indexOf(name) == -1) continue;
        var val = params[i].substring(params[i].indexOf('=') + 1);
        while (val.indexOf('+') != -1) val = val.replace('+', ' ');
        var child = document.createElement('p');
        child.className = 'SearchCriteria';
        if (name == 'pool' || name == 'pets' || name == 'garage' || name == 'complex')
            child.innerHTML = ((name == 'pets') ? 'Allows ' : (name == 'complex') ? 'Is ' : 'Has ') + name;
        else
            child.innerHTML = name.charAt(0).toUpperCase() + name.substring(1) + ((name == 'price') ? ' ≤ R' : ': ') + val;
        criteria.appendChild(child);
        FillInputBox(name, val);
    }
}

function FillInputBox(name, val) {
    var element = document.getElementsByName(name)[0];
    if (name == 'garage' || name == 'pool' || name == 'pets' || name == 'complex')
        element.checked = true;
    else
        element.value = val
}

function RecordHover(digsID, record) {
    var element = record.parentElement.parentElement;
    var otherElements = document.getElementsByClassName('digsRecord');
    for (var i = 0; i < otherElements.length; i++)
        otherElements[i].parentElement.parentElement.style.backgroundColor = 'white';
    element.style.backgroundColor = '#F0F0F0';
}

function InsertRatings() {
    var digsIDs = [];
    var digsRatings = [];
    var records = [].slice.call(document.querySelectorAll('#Ratings form tr')).filter(function (x) { return x.className.includes('cbResultSet') && x.className.includes('Row') });
    for (var i = 0; i < records.length; i++) {
        var id = ''; var rate = '';
        var nodes = records[i].children;
        for (var a = 0; a < nodes.length; a++) {
            if (nodes[a].className.includes('cbResultSetTableCellNumberDate'))
                rate = nodes[a].innerText.trim();
            else id = nodes[a].innerText.trim();
        }
        if (id != '') {
            digsIDs.push(id);
            digsRatings.push(rate);
        }
    }
    var list = document.getElementsByClassName('DigsRating');
    for (var i = 0; i < list.length; i++) {
        var id = list[i].children[0].innerText.trim();
        list[i].children[1].style.display = 'none';
        var index = digsIDs.indexOf(id);
        var stars = document.createElement('div');
        if (index != -1) {
            var score = (Math.round(digsRatings[index] * 2) / 2).toFixed(1)
            for (var j = 0; j < 5; j++) {
                var star = document.createElement('img');
                star.className = 'Star';
                if (score >= 1.0) {
                    star.src = '../Images/FullStar.png';
                    star.alt = 'Full Star';
                    score -= 1;
                }
                else if (score >= 0.5) {
                    star.src = '../Images/HalfStar.png';
                    star.alt = 'Half Star';
                    score -= 0.5;
                }
                else {
                    star.src = '../Images/EmptyStar.png';
                    star.alt = 'Empty Star';
                }
                stars.appendChild(star);
            }
        }
        else {
            var element = document.createElement('p');
            element.innerHTML = 'No Reviews';
            stars.appendChild(element);
        }
        list[i].appendChild(stars);
    }
}