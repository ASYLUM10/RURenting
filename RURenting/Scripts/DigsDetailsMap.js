﻿var map;
var markers;
var UnselectedPin;
var SelectedPin;

function initializeMap() {
    UnselectedPin = {
        url: "http://darqdev.co.za/Images/Unselected.png",
        size: new google.maps.Size(32, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 32)
    };
    var myLatlng = new google.maps.LatLng(-33.315133, 26.5207898);
    var myOptions = {
        zoom: 15,
        center: myLatlng,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), myOptions);
    addMarker();
}

function addMarker() {
    var digs = document.getElementsByClassName("Location");

    var digsID = "";
    var lat = 0.0;
    var lng = 0.0;


    for (var i = 0; i < digs.length; i++) {
        var nodes = digs[i].children;

        for (var j = 0; j < nodes.length; j++) {
            if (nodes[j].id == "DigsID") {

                digsID = nodes[j].innerText.trim();
            }
            else if (nodes[j].id == "Latitude") {
                lat = nodes[j].innerText.trim();
            }
            else if (nodes[j].id == "Longitude") {
                lng = nodes[j].innerText.trim();
            }
        }
        placeMarker(new google.maps.LatLng(lat, lng), digsID, UnselectedPin);
    }
}

function placeMarker(location, _digsID, _pinImage) {
    _marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: _pinImage,
    });
    markers = _marker
}

