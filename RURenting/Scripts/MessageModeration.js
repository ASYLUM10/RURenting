﻿function BlacklistMessages() {
    var blacklist = document.getElementById('cbParamVirtual1').checked;
    if (blacklist)
        document.getElementById('EditRecordMessage_isActive').checked = false;
    return true;
}

function RemoveFlag() {
    var confirmation = confirm("Are you sure you wish you remove this flag?");
    if (confirmation) {
        document.getElementById('EditRecordisFlagged').checked = false;
        document.getElementById('RemoveFlagForm').getElementsByTagName('form')[0].submit();
    }
    return false;
}

document.getElementById('RemoveFlag').onclick = function () { return RemoveFlag() };
document.getElementById('caspioform').onsubmit = function () { return BlacklistMessages() };