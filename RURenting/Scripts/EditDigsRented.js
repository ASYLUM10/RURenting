﻿function checkDate() {
    var currentDate = Date.now();
    var movedIn = Date.parse(document.getElementById('RecordDateMovedIn').innerHTML);
    var movedOut = Date.parse(document.getElementById('EditRecorddateMovedOut').value);
    if (!isNaN(movedOut)) {
        var timeDiff = Math.abs(movedOut - movedIn);
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (movedOut > currentDate) {
            window.alert("Date Moved Out cannot be after the present date.");
            return false;
        }
        else if (movedOut <= movedIn) {
            window.alert("Date Moved In must be before the date Moved Out.");
            return false;
        }
        else if (diffDays < 7) {
            window.alert("The minimum time period for staying in a digs is 7 days.");
            return false;
        }
    }
    var digsRented = document.getElementsByClassName("RentedDigs");
    return true;
}

document.getElementById('caspioform').onsubmit = function () { return (checkDate()) };