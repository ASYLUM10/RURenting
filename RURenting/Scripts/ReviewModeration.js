﻿function RemoveFlag() {
    var confirmation = confirm("Are you sure you wish you remove this flag?");
    if (confirmation) {
        document.getElementById('EditRecordisFlagged').checked = false;
        document.getElementById('RemoveFlagForm').getElementsByTagName('form')[0].submit();
    }
    return false;
}

document.getElementById('RemoveFlag').onclick = function () { return RemoveFlag() };