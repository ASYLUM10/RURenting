﻿function HideShow(id, element) {
    var style = document.getElementById(id).style.display;
    if (style == "none") {
        element.innerHTML = element.innerHTML.substr(0, element.innerHTML.length - 3) + "[-]";
        document.getElementById(id).style.display = "block";
    }
    else {
        element.innerHTML = element.innerHTML.substr(0, element.innerHTML.length - 3) + "[+]";
        document.getElementById(id).style.display = "none";
    }
}