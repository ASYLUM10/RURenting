<h1>RURenting:</h1>
Sys Dev 2016

<h2>Participants:</h2>
* Damon Hook
* Marq Botha
* Sam Langton

<h2>Set Up:</h2>
* Have some program that will allow you to use git commands, I recommend CMDER: http://cmder.net/

<h2>Commands:</h2>
<h3>Proxy:</h3>
* On Lab computers: **`git config --global http.proxy http://username:password@wwwproxy.ru.ac.za:3128`**

<h3>Getting the Repository:</h3>
* Clone Repo: **`git clone https://gitlab.com/asylum10/rurenting.git`**
* Getting the latest version of an already clones repo: **`git pull`**

<h3>Saving changes:</h3>
* Go into the folder where the files are (will normally be called Cs301Compilers): **`git add -A`**
* Commit the changes: **`git commit -m "type a commit message"`**
* Push onto the git: **`git push`** - Then put in username and password (password is typing even though you can't see it).